#!/bin/sh

# Hacky solution to generate the README.md from vapid's usage ouput

(
echo "# Vapid"
echo "**vapid** is a wrapper for the python3 **venv** and **pip** modules which fits my needs. You should probably use **pipenv** or **poetry** or something, and perhaps I should too."
echo
echo "## Installation"
echo Download the file named **vapid** and put it on your path somewhere.
echo
echo "## Usage"

vapid                                       \
| sed 's/"$//g'                             \
| sed 's/^usage:/>/g'                       \
| sed 's/$(basename $0)/vapid/g'            \
| sed 's/ACTION/**ACTION**/'                \
| sed -E 's/^    ([a-z]+)( +)/- **\1**\2/g' \
| sed 's/^  When/*When/g'                   \
| sed 's/venv:$/venv:*/g'                   \
| sed -E 's/(pip_[a-z]+)/*\1*/g'

echo "## Example"
echo '```'
echo '$ git clone https://example.com/git/a_very_exciting_python_program'
echo '$ cd a_very_exciting_python_program'
echo '$ vapid new'
echo '$ vapid activate'
echo '$ vapid install -r requirements.txt'
echo '$ ./a_very_exciting_python_program'
echo '```'

echo "## TODO"
echo "- I have written actions for building and publishing python packages to pypi etc, but I need to refamiliarize myself with it before I release it. (It's currently just commented out)"
echo "- Support for more shells when activating (Or just do the activate in a much better way that works for all shells?)"
echo "- Passing arguments through to backend tools pip_autoremove and pip_upgrade_outdated."
echo "- Options (file/cmdline) for changing certain behaviours that might not be wanted."

) | tee README.md
