# Vapid
**vapid** is a wrapper for the python3 **venv** and **pip** modules which fits my needs. You should probably use **pipenv** or **poetry** or something, and perhaps I should too.

## Installation
Download the file named vapid and put it on your path somewhere.

## Usage
> vapid **ACTION** [ACTION_ARGS...]

Where **ACTION** is one of the following:

*When NOT in a venv:*
- **new**      [VENV]           - creates a new venv with name VENV if provided, else name of current directory
- **activate** [VENV]           - activates an existing venv with name VENV if provided, else name of current directory
- **delete**   VENV             - deletes an existing venv
- **run**      VENV [ARGS...]   - run the python3 within venv with name VENV 

*When in a venv:*
- **install**   PIPARGS...      - install a package into the active venv using pip. All subsequent arguments are passed through to pip. Prior to this it will upgrade pip and install wheel and setuptools to make things easier.
- **list**      PIPARGS...      - list installed packages. All subsequent arguments are passed through to pip.
- **uninstall** PACKAGE         - uninstall a package form the active venv. This will install and run the third-party *pip_autoremove* package.
- **upgrade**                   - upgrade outdated packages. This will install and run the third-party *pip_upgrade*_outdated package.

## Example
```
$ git clone https://example.com/git/a_very_exciting_python_program
$ cd a_very_exciting_python_program
$ vapid new
$ vapid activate
$ vapid install -r requirements.txt
$ ./a_very_exciting_python_program
```
## TODO
- I have written actions for building and publishing python packages to pypi etc, but I need to refamiliarize myself with it before I release it. (It's currently just commented out)
- Support for more shells when activating (Or just do the activate in a much better way that works for all shells?)
- Passing arguments through to backend tools pip_autoremove and pip_upgrade_outdated.
- Options (file/cmdline) for changing certain behaviours that might not be wanted.
